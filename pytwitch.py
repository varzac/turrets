#!/usr/bin/python
import requests
import json
import npyscreen
import urllib
import subprocess
import os


FNULL = open(os.devnull, 'w')


class pytwitch(npyscreen.NPSAppManaged):
    def onStart(self):
        m = self.addForm('MAIN', MainPage)
        g = self.addForm('GAME', GamePage)
        s = self.addForm('STREAMS', StreamPage)


class MainPage(npyscreen.Form):
    def create(self):
        self.add(npyscreen.ButtonPress, name='Games', when_pressed_function=self.switch_to_games)

    def switch_to_games(self):
        self.parentApp.setNextForm('GAME')
        self.editing = False


class StreamPage(npyscreen.Form):
    def add_list(self, stream_list):
        for name, game, viewers in stream_list:
            self.add(npyscreen.ButtonPress, name='%s playing %s: %s viewers' % (name, game, viewers), when_pressed_function=self.create_bound_load(name))

    def search(self, search):

        url = 'https://api.twitch.tv/kraken/search/streams?q=%s' % urllib.quote_plus(search)
        r = requests.get(url)
        js = json.loads(r.content)

        strims = []
        for stream in js['streams']:
            strims.append((stream['channel']['name'], stream['game'], stream['viewers']))

        self.add_list(strims)

    def create_bound_load(self, name):
        def func():
            self.load_stream(name)

        return func

    def load_stream(self, name, quality='high'):
        subprocess.Popen(['livestreamer', 'twitch.tv/%s' % name, quality], stdout=FNULL)


class GamePage(npyscreen.Form):
    def __init__(self, *args, **kwargs):
        npyscreen.Form.__init__(self, *args, **kwargs)
        self.next_url = None

    def create(self):
        r = requests.get('https://api.twitch.tv/kraken/games/top')
        games = json.loads(r.content)

        for game in games['top']:
            name = game['game']['name']
            self.add(npyscreen.ButtonPress, name='%s has %s viewers' % (name, game['viewers']), when_pressed_function=self.create_bound_stream_switch(name))

        self.add(npyscreen.Button, name='Next 10 Games')
        self.next_url = games['_links']['next']

    def create_bound_stream_switch(self, search):
        def func():
            s = self.parentApp.getForm('STREAMS')
            s.search(search)
            self.parentApp.setNextForm('STREAMS')
            self.editing = False

        return func


if __name__ == "__main__":
    my_app = pytwitch().run()